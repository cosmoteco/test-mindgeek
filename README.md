Installation instructions

	1. clone the repo:

git clone https://cosmoteco@bitbucket.org/cosmoteco/test-mindgeek.git

	2. setup environment:
docker-compose --compatibility up --build   (Wait until exits with composer code 0. Ignore warnings on queue. Retries queue deploy until composer is done)

	OR 
docker-compose --compatibility up --build -d (Detached mode. Wait one minut in order to download all packages)

	3. open URL

http://localhost:8080/demo?page=1


Run unit tests:

- docker-compose --compatibility exec app  php ./sample/vendor/bin/phpunit sample/tests/CacheHandlerTest.php
- docker-compose --compatibility exec app  php ./sample/vendor/bin/phpunit sample/tests/ProcessImageJobTest.php


About

- Application used to download images from a given source using paralel processing

Technologies

- Docker for deployment
- Lumen framework
- Lumen Job feature for paralel processing
- Beanstalkd for queueing management
- Redis for cache management
- Composer as a package manager 
- PHPunit for unit testing

Possible issues:

- Too much memory consumption. Reduce number of queues from docker-compose.yml. Now it's set to 10
- Port 80 might be used on your network. Change in docker-compose.yml the port on web image. For example "8084:84" and call http://localhost:8084/demo or http://locahost:84/demo
- Composer might not download packages: Override --mtu=BYTES. Now it's 1450. It might work with 1500
- Warning: require_once(/var/www/sample/bootstrap/../vendor/autoload.php): failed to open stream: No such file or directory in /var/www/sample/bootstrap/app.php on line 3
  You didn't wait enough for the deploy to finish composer install. The queues are depending on vendor.
