<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/demo','SampleController@demo');

$router->get('/publish', 'SampleController@publish');

$router->post('/register','SampleController@register');

/** get email older than a year */
$router->get('/emails/marked', 'SampleController@emails');
$router->get('/emails/limit/{limit}/offset/{offset}', 'SampleController@emails');

$router->get('/', function () use ($router) {
    return $router->app->version();
});
