<?php $keys = array_keys($item); ?>
<ul class="parent-list">
    <?php foreach ($keys as $key => $name): ?>
        <?php
        if ('cardImages' === $name) {
            include('carousel.php');
            continue;
        }
        if ('keyArtImages' === $name) {
            include('carousel.php');
            continue;
        }
        try {
            if (is_array($item[$name])) {
                include 'firstLevel.php';
            } else {
                ?>
                <li class="item"><label class="label"><?php echo $name; ?>:</label>&nbsp;<?php echo $item[$name]; ?>
                </li>
                <?php
            }
        } catch (Exception $e) {
            throw new Exception('Render exception');
        }
    endforeach;
    ?>
</ul>
