<?php

namespace App\Internal;

use App\Jobs\ProcessImageJob;

class ToolService {
	public static function hashString(string $string): string {
		return md5($string);
	}

	public static function hashTime(string $date): string {
		return strtotime($date);
	}

    /**
     * @param string $url
     *
     * @return array
     */
	public static function getCurlData(string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $content = curl_exec($ch);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

        curl_close($ch);

        return [$content, $content_type];
    }

    /**
     * @param string $path
     * @param $content
     *
     * @return string
     */
    public static function saveToDisk(string $path, $content)
    {
        if (file_exists($path)) {
            return $path;
        }

        file_put_contents($path, $content);

        return $path;
    }

    public static function getFileExtension(string $url): string
    {
        $ext = pathinfo($url, PATHINFO_EXTENSION);

        return '.' . $ext;
    }
}

