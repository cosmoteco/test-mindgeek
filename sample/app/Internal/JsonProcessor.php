<?php

namespace App\Internal;

use App\Internal\CacheHandler as Cache;
use App\Internal\ToolService as ToolService;

class JsonProcessor {
	const JSON_URL = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';
	const JSON_CACHE_EXPIRE = 3600; // arbitrary added JSON expire time: 1 hour
	const JSON_LAST_UPDATED_KEY = 'lastUpdated';
	const JSON_IMAGE_KEY = [
		'cardImages',
		'keyArtImages',
	];
	const JSON_IMAGE_CONTENT_TYPE = [
		'image/jpeg',
		'image/gif',
		'image/png',
	];

	public function __construct()
	{
	}

	public static function getJson(string $url): string
	{
		return file_get_contents($url);
	}

	public static function getDecodedContent(string $jsonData): array
	{
		$escapedJsonData = utf8_encode($jsonData);

		$jsonDecodedData = json_decode($escapedJsonData, true);

		return $jsonDecodedData;
	}
}