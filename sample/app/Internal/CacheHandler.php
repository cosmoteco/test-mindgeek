<?php

namespace App\Internal;

use App\Internal\ToolService as ToolService;
use App\Internal\JsonProcessor as JsonProcessor;

use Illuminate\Support\Facades\Queue;
use App\Jobs\BeanstalkdJob;
use App\Jobs\ProcessImageJob;

class CacheHandler {
	public function cacheJson(string $url, string $content): string
	{
		$urlCacheKey = ToolService::hashString($url);

		app('redis')->set($urlCacheKey, $content);
		app('redis')->expire($urlCacheKey, JsonProcessor::JSON_CACHE_EXPIRE);

		return app('redis')->get($urlCacheKey);
	}

	public static function generateImageCacheKey(string $url, string $date): string
	{
		$urlHash = ToolService::hashString($url);
		$timeHash = ToolService::hashTime($date);

		return $urlHash . '_' . $timeHash;
	}

	public function downloadAndCache(array $decodedJson): void
	{
		foreach ($decodedJson as $key => $item) {
			// used for cache invalidation
			$lastUpdated = $item[JsonProcessor::JSON_LAST_UPDATED_KEY];

			$images = array_merge_recursive(
				$item[JsonProcessor::JSON_IMAGE_KEY[0]],
				$item[JsonProcessor::JSON_IMAGE_KEY[1]]
			);

            Queue::push(new ProcessImageJob(['data' => ['images' => $images, 'lastUpdated' => $lastUpdated]]));
		}

	}

    public function getDataToRender(array $items): array
    {
        foreach ($items as &$item) {
            $cardImages = &$item[JsonProcessor::JSON_IMAGE_KEY[0]];
            foreach ($cardImages as &$image) {

                $imageKey = $this->generateImageCacheKey(
                    $image['url'],
                    $item[JsonProcessor::JSON_LAST_UPDATED_KEY]
                );
                if (app('redis')->exists($imageKey)) {
                    $image['cache'] = app('redis')->get($imageKey);
                }
            }

            $keyArtImages = &$item[JsonProcessor::JSON_IMAGE_KEY[1]];
            foreach ($keyArtImages as &$image) {
                $imageKey = $this->generateImageCacheKey(
                    $image['url'],
                    $item[JsonProcessor::JSON_LAST_UPDATED_KEY]
                );

                if (app('redis')->exists($imageKey)) {
                    $image['cache'] = app('redis')->get($imageKey);
                }
            }

        }

        return $items;
    }
}

