<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Queue;
use App\Jobs\BeanstalkdJob;
use App\Jobs\ProcessImageJob;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Internal\JsonProcessor;
use App\Internal\CacheHandler;
use App\Internal\ToolService as ToolService;

class SampleController extends Controller
{

	const JSON_URL = JsonProcessor::JSON_URL;
	const ITEMS_PER_PAGE =5;

	/** @var JsonProcessor */
	private $jsonProcessor;

	/** @var CacheHandler */
	private $cacheHandler;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    	public function __construct($cache = null, $json = null)
	{
		$this->cacheHandler = $cache;
		$this->jsonProcessor = $json;
	}

	public function demo(Request $request)
	{
		$currentPage = $request->input('page');

		try {
			$cachedJson = '';

			if (app('redis')->exists(ToolService::hashString(self::JSON_URL))) {
				$cachedJson = app('redis')->get(ToolService::hashString(self::JSON_URL));
			}

			if (!app('redis')->exists(ToolService::hashString(self::JSON_URL))) {
				$jsonData = JsonProcessor::getJson(static::JSON_URL); // get JSON from url
				$cachedJson = $this->getCacheHandler()->cacheJson(static::JSON_URL, $jsonData);

			}

			$decodedJson = JsonProcessor::getDecodedContent($cachedJson);

			//send all content for caching to be processed in parallel
			$this->getCacheHandler()->downloadAndCache($decodedJson);

			// used to only create cache for current page
            $paginator = self::paginate($decodedJson, $currentPage);
            $paginator->setPath('demo');

            $data = $this->cacheHandler->getDataToRender($paginator->items());
		} catch (\Exception $e) {
			return response($e->getMessage(), 503); // Service Unavailable
		}

		return view(
			'show',
			[
				'data' => $data,
				'paginator' => $paginator,
			]
		);
	}

    /**
     * {
     *  "data":{
     * "clientIndentifier1":["test@mail.com","test2@email.com"],
     *  "clientIndentifier2":["rerere"]
     * }
     * }
     * @param Request $request
     */
	public function register(Request $request) {
        $data = $request->input('data');
        try {
            foreach ($data as $clientIdentif => $emails) {
                foreach ($emails as $emailString) {
                    /*$email = new Email();
                    $email->setAddress($emailString)
                        ->setClientIdentifier($clientIdentif);*/
                    $insertedEmails[$emailString] = $emailString;
                    DB::insert('insert into emails (address,client_identifier) values (?, ?)', [mysql_real_escape_string($emailString), mysql_real_escape_string($clientIdentif)]);
                }
            }
        } catch (\Exception $ex) {
            http_response_code(500);
            return json_encode(
                [
                    'success'=> false,
                    'message' => 'Internal server error'
                ]
            );
        }
        http_response_code(200);
        return json_encode([
            'success'=>true,
            'message' => $insertedEmails
        ]);
    }

    /**
     * @param Request $request
     */
    public function emails(Request $request)
    {
        $limit = !empty($request->input('limit')) ? $request->input('limit') : 10;
        $offset = $request->input('offset') ?? 0;

        try {
//            if (Is marked route) {
//                // select emails older than a year
//            }

            //It's not enough. Also by using an ORM we wouldn't need to worry about escaping.
            $results = DB::select('select e.id, e.address, e.client_identifier from emails e limit :offset,:limit', ['offset' => mysql_real_escape_string($offset), 'limit' => mysql_real_escape_string($limit)]);
            foreach ($results as $result) {
                $emails[$result['id']] = [
                    'clientIdentifier' => $result['clientIdentifier'],
                    'address' => $result['address']
                ];
            }
        } catch (\Exception $ex) {
            http_response_code(500);
            return json_encode(
                [
                    'success'=> false,
                    'message' => 'Internal server error'
                ]
            );
        }

        http_response_code(200);
        return json_encode([
            'success'=>true,
            'message' => $emails
        ]);
    }

	public static function paginate($processedJson, $currentPage)
	{
		return new Paginator(
			self::getCurrentItems($processedJson, $currentPage),
			count($processedJson),
			self::ITEMS_PER_PAGE,
			$currentPage
		);
	}

	protected function getJsonProcessor()
	{
		if (null === $this->jsonProcessor) {
			$this->jsonProcessor = new JsonProcessor();
		}
		return $this->jsonProcessor;
	}

	protected function getCacheHandler()
	{
		if (null === $this->cacheHandler) {
			$this->cacheHandler = new CacheHandler();
		}

		return $this->cacheHandler;
	}

	private static function getCurrentItems($items, $currentPage): array
	{
		// Get current items calculated with per page and current page
		return array_slice($items, self::ITEMS_PER_PAGE * ($currentPage - 1), self::ITEMS_PER_PAGE);
	}
}
