<?php

namespace App\Jobs;

use App\Internal\ToolService;
use App\Jobs\Job;
use Log;
use Illuminate\Http\Request;

use App\Internal\JsonProcessor as JsonProcessor;
use App\Internal\CacheHandler as CacheHandler;

class ProcessImageJob extends Job
{
    const IMAGE_CONTENT_TYPE = [
        'image/jpeg',
        'image/gif',
        'image/png',
    ];

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data['data'];
    }

    /**
     * Execute the job.
     *
     * @param
     * @return void
     */
    public function handle()
    {
        $images = $this->data['images'];
        $dateModified = $this->data['lastUpdated'];

        foreach ($images as $key => $image) {
            $imageCacheKey = CacheHandler::generateImageCacheKey($image['url'], $dateModified);

            if (app('redis')->exists($imageCacheKey)) {
                continue;
            }

            $result = static::file_get_contents_curl($image['url']);

            $cacheFilePath = static::saveImage($imageCacheKey, $result['extension'], $result['content']);

            app('redis')->set($imageCacheKey, $cacheFilePath);
        }
    }

    private static function saveImage(string $name, string $extension, $content): string
    {
        if (null === $content) {
            return 'image-not-found.jpg';
        }

        $path = env('CACHE_IMAGE_FOLDER') . $name . $extension;

        return ToolService::saveToDisk($path, $content);
    }


    public static function file_get_contents_curl(string $url): array
    {
        [$data, $content_type] = ToolService::getCurlData($url);

        if (\in_array($content_type, static::IMAGE_CONTENT_TYPE)) {
            return [
                'extension' => ToolService::getFileExtension($url),
                'content' => $data,
            ];
        }

        return [
            'extension' => ToolService::getFileExtension($url),
            'content' => null,
        ];
    }
}
