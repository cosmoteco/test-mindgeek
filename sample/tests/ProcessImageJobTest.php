<?php
/**
 * Created by PhpStorm.
 * User: cosmin.croitoru
 * Date: 9/15/2019
 * Time: 11:00 PM
 */

use App\Jobs\ProcessImageJob;

class ProcessImageJobTest extends TestCase
{
    protected $toolServiceMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->toolServiceMock = Mockery::mock('alias:\App\Internal\ToolService');
    }

    public function setTestSaveImageData()
    {
        return [
            [
                'values' => [
                    'data' => [
                        'images' => [
                            'key1' => [
                                'url' => 'testing/url/image.jpg'
                            ]

                        ],
                        'keyExists' => false,
                        'name' => 'test',
                        'extension' => 'image/jpeg',
                        'content' => 'testcontent',
                        'lastUpdated' => '2019-01-01'
                    ]
                ],
                'expected' => [
                    'public/image/cache/test.jpg'
                ]
            ], [
                'values' => [
                    'data' => [
                        'images' => [
                            'key1' => [
                                'url' => 'testing/url/image.jpg'
                            ]

                        ],
                        'keyExists' => true,
                        'name' => 'test',
                        'extension' => 'image/jpeg',
                        'content' => 'testcontent',
                        'lastUpdated' => '2019-01-01'
                    ]
                ],
                'expected' => []
            ]
        ];
    }

    /** @dataProvider setTestSaveImageData */
    public function testSaveImage($values, $expected)
    {

        parent::setUp();
        $processImageJob = new ProcessImageJob($values);

        $redis = new \Illuminate\Support\Facades\Redis();
        app()->instance(\Illuminate\Support\Facades\Redis::class, $redis);

        $redis->shouldReceive('exists')
            ->with(Mockery::type('string'))
            ->andReturn($values['data']['keyExists']);

        $this->toolServiceMock->shouldReceive('hashString')
            ->with(Mockery::type('string'))
            ->andReturn('testtest');

        $this->toolServiceMock->shouldReceive('hashTime')
            ->with(Mockery::type('string'))
            ->andReturn('hashTimeTest');

        $this->toolServiceMock->shouldReceive('getFileExtension')
            ->with(Mockery::type('string'))
            ->andReturn($values['data']['extension']);

        $this->toolServiceMock->shouldReceive('saveToDisk')
            ->withArgs(
                [
                    Mockery::type('string'),
                    Mockery::type('string')
                ]
            )
            ->andReturn(env('CACHE_IMAGE_FOLDER').$values['data']['name'].$values['data']['extension']);

        $this->toolServiceMock->shouldReceive('getCurlData')
            ->with(Mockery::type('string'))
            ->andReturn([$values['data']['content'], $values['data']['extension']]);

        if (!$values['data']['keyExists']) {
            $redis->shouldReceive('set')
                ->withArgs(
                    [
                        Mockery::type('string'),
                        Mockery::type('string')
                    ]
                )
                ->andReturn(true);
        }

        $processImageJob->handle($values);
    }
}